from __future__ import print_function
import flask
import sys
import phonenumbers
import json
from phonenumbers import geocoder, carrier

from twilio.rest import Client
from flask import request, jsonify, redirect

from six.moves.urllib.request import urlopen
from functools import wraps

from flask import Flask, request, jsonify, _request_ctx_stack
from flask_cors import cross_origin
from jose import jwt

from Authentication import requires_auth
import http.client



application = flask.Flask(__name__)
application.config["DEBUG"] = True

account_sid = "AC5980c85ba998810bb88018bc2f13c94b"
auth_token = "9a1bb09e4bde79011d502931baf7bba6"

client = Client(account_sid, auth_token)
#msg = 'Please enter a Valid Phone Number'
number_type_dict = {0: "FIXED_LINE",
                    1: "MOBILE",
                    2: "FIXED_LINE_OR_MOBILE",
                    3: "TOLL_FREE",
                    4: "PREMIUM_RATE",
                    5: "SHARED_COST",
                    6: "VOIP",
                    7: "PERSONAL_NUMBER",
                    8: "PAGER",
                    9: "UAN",
                    10: "VOICEMAIL",
                    99: "UNKNOWN"
                    }

def line_type(ph_number):
    ph_number_details = client \
        .lookups \
        .phone_numbers(ph_number) \
        .fetch(type=['carrier'])
    return ph_number_details.carrier
  
def phone_number_validation(phone_number,country_code):
    country_code = country_code.upper()
    parse_phone_number = phonenumbers.parse(phone_number, country_code)
    if phonenumbers.is_possible_number(parse_phone_number) and phonenumbers.is_valid_number(parse_phone_number):
        national = phonenumbers.format_number(parse_phone_number, phonenumbers.PhoneNumberFormat.NATIONAL)
        international = phonenumbers.format_number(parse_phone_number,phonenumbers.PhoneNumberFormat.INTERNATIONAL)
        phone_number_type = number_type_dict[phonenumbers.number_type(parse_phone_number)]
        return {'valueInNationalFormat': national, 'valueInInternationFormat': international,
                       'type': phone_number_type, 'isValidNumber': True, 'value': phone_number}
    else:
        return {"value": phone_number, "isValidNumber": False}



@application.route('/')
@cross_origin(headers=["Content-Type", "Authorization"])
def home():
    return '''<h1>Phone Number Lookup</h1>
<p>A prototype API for phone number lookup</p>'''

@application.route('/api/phonenumber', methods=['GET'])
@cross_origin(headers=['Content-Type', 'Authorization'])
@requires_auth    
def api_phno():
    phone_number = request.args['phone_number']
    country_code = request.args['country_code']
    if(country_code == 'co_uk' or country_code == 'uk'):
        country_code = 'gb'
    #return jsonify(phone_number_validation(phone_number,country_code))
    #testreturn = phone_number+country_code
    json_data = phone_number_validation(phone_number,country_code)
    isvalid_number = json_data["isValidNumber"]
    #return json_data
    #return str(isvalid_number)
    
    if isvalid_number:
        type_of_number = json_data["type"]
        type_of_number = type_of_number.lower()
        if(type_of_number == 'mobile'):
            international_phone_number = json_data["valueInInternationFormat"]
            return (jsonify(line_type(international_phone_number)))
        else:
            return json.dumps({"phone_number_type":"Fixed_Line or Landline"})
    else:
        return json.dumps({"isvalid_number":"False"})
        
application.run()